<?php

namespace Controller;

use App\Http\Controllers\TurnController;
use App\Models\Doctor;
use App\Models\Turn;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Tests\TestCase;

class TurnControllerTest extends TestCase
{
    private string $errorUserHasTurn = '{"error":{"title":"turn failed","description":"user have turn in-progress for this doctor","details":"user not given turn"}}';
    private string $errorDoctorFullTurn = '{"error":{"title":"turn failed","description":"capacity_doctor_is_full","details":"user not given turn"}}';


    public function testCreate()
    {
        $controller = new TurnController();
        $request = app(Request::class);

        $request->merge([
            'user_id' => 4,
            'doctor_id' => 2
        ]);

        $response = $controller->create($request);

        $this->assertJsonStringEqualsJsonString(
            '{"user_id":'.json_decode($response->getContent(), true)['user_id'].',"doctor_id":'.json_decode($response->getContent(), true)['doctor_id'].',"turn_id":' . json_decode($response->getContent(), true)['turn_id'] . '}',
            $response->getContent()
        );
    }

    public function testFailedWithUserHasTurn()
    {
        $this->addTurnForUser();
        $controller = new TurnController();
        $request = app('request');

        $request->merge([
            'user_id' => 4,
            'doctor_id' => 2
        ]);

        $response = $controller->create($request);
        $this->assertJsonStringEqualsJsonString($this->errorUserHasTurn, $response->getContent());
    }

    public function testCreateTurnFailedWhenDoctorIsFull()
    {
        $this->changeReservationDoctor();
        $controller = new TurnController();
        $request = app('request');

        $request->merge([
            'user_id' => 4,
            'doctor_id' => 3
        ]);

        $response = $controller->create($request);
        $this->assertJsonStringEqualsJsonString($this->errorDoctorFullTurn, $response->getContent());
    }


    private function addTurnForUser()
    {
        Turn::query()->create([
            'user_id' => 1,
            'doctor_id' => 1
        ]);
    }

    private function changeReservationDoctor()
    {
        Doctor::query()->where('id', 3)
            ->update([
                'capacity' => 1,
                'reservation' => 1
            ]);
    }
}
