<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Console\Command;

class SendRequestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command For Send 100 Request';

    /**
     * Execute the console command.
     */

    private int $allowableCeiling = 100;

    public function handle(): void
    {
        $number = 1;
        while ($number <= $this->allowableCeiling){
            $client = new Client();
            $url = env('APP_URL');
            $body = ['user_id' => rand(1,10), 'doctor_id' => rand(1, 20)];
            $client->post($url.'/turn', $body);
            echo $number;
            $number++;
        }
    }
}
